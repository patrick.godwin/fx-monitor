/*
 * Copyright (C) 2016  Kipp Cannon, Chad Hanna
 * 
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 * Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */


/*
 * Variables with global scope
 */


var latency_per_time_wrapper;
var percent_missed_per_time_wrapper;
var target_snr_per_time_wrapper;
var charts = [];

var H1="#F5B7B1";
var L1="#A9CCE3";
var V1="#D2B4DE";
var ALL="#A2D9CE";
var white="#FBFCFC";
var darkblue="#2c3e50";

default_options = {
	title: 'Figure', 
	hAxis: { gridlines: {color:darkblue}, textStyle: {color:darkblue} },
	vAxis: { textStyle: {color: darkblue}, textPosition: 'out', viewWindowMode:'explicit', viewWindow:{min:0, max:100}, gridlines: {color:darkblue}},
	chartArea: {left:50, top:15, width:'95%', height:'70%', backgroundColor:white},
	titlePosition: 'in',
	titleTextStyle: {color: darkblue},
	series: {0: {color: H1}, 1: {color: L1}, 2:{color: V1}, 3:{color: ALL}},
	legend: {position: "in", textStyle: {color: darkblue}},
	explorer: {actions: ['dragToZoom', 'rightClickToReset']},
	dataOpacity: "0.9",
	curveType: "none",
	fontName: "M PLUS Rounded 1c",
	fontSize: 12,
	lineWidth: 2,
	backgroundColor: {stroke: darkblue, fill: white, strokeWidth: '1'},
	width: "90%",
	bar: {
	    groupWidth: '70%',
	},
	allowHtml: true,
	tooltip: {
		isHtml: true
	}
};


/*
 * Utility function
 */


function clone(obj) {
	var copy;

	// Handle the 3 simple types, and null or undefined
	if (null == obj || "object" != typeof obj) return obj;

	// Handle Date
	if (obj instanceof Date) {
		copy = new Date();
		copy.setTime(obj.getTime());
		return copy;
	}

	// Handle Array
	if (obj instanceof Array) {
		copy = [];
		for (var i = 0, len = obj.length; i < len; i++) {
			copy[i] = clone(obj[i]);
		}
		return copy;
	}

	// Handle Object
	if (obj instanceof Object) {
		copy = {};
		for (var attr in obj) {
			if (obj.hasOwnProperty(attr)) copy[attr] = clone(obj[attr]);
		}
		return copy;
	}

	throw new Error("Unable to copy obj! Its type isn't supported.");
}

function openGstlalTab(evt, tabName) {
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	// Disable automatic queries
	for (i = 0; i < charts.length; i++) {
		charts[i].setRefreshInterval(0);
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the link that opened the tab
	document.getElementById(tabName).style.display = "block";
	evt.currentTarget.className += " active";

	// Redraw and re-enable queries for this chart
	for (i = 2; i < arguments.length; i++) {
		arguments[i].clear();
		// FIXME allow this function to accept a custom refresh
		// interval for each chart instead of this hardcoded BS
		arguments[i].setRefreshInterval(longrefresh);
		arguments[i].draw();
        }
}

function updateClock ( )
 	{
 	var currentTime = new Date ( );
  	var currentHours = currentTime.getHours ( );
  	var currentMinutes = currentTime.getMinutes ( );
  	var currentSeconds = currentTime.getSeconds ( );

  	// Pad the minutes and seconds with leading zeros, if required
  	currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
  	currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

  	// Choose either "AM" or "PM" as appropriate
  	var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

  	// Convert the hours component to 12-hour format if needed
  	currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

  	// Convert an hours component of "0" to "12"
  	currentHours = ( currentHours == 0 ) ? 12 : currentHours;

  	// Compose the string for display
  	var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
  
	var timeInMs = "" + Math.floor((Date.now() - 315964800000 + 17000)/1000.) + "&nbsp;" + currentTimeString;
  	$("#clock").html(timeInMs);
   	//$("#clock").html(currentTimeString);
   	  	
 }

$(document).ready(function()
{
	setInterval('updateClock()', 1000);
	//$(".fancybox").fancybox();
	//openGstlalTab(event, 'Status', time_since_last_wrapper, time_since_trigger_wrapper, up_time_wrapper, dropped_wrapper, ram_status_wrapper);
});


/*
 * Charts about latency
 */


/*
 * https://developers.google.com/chart/interactive/docs/reference#Query
 */

var QueryWrapper = function(query, visualization, visOptions, errorContainer) {

  this.query = query;
  this.visualization = visualization;
  this.options = visOptions || {};
  this.errorContainer = errorContainer;
  this.currentDataTable = null;

  if (!visualization || !('draw' in visualization) ||
      (typeof(visualization['draw']) != 'function')) {
    throw Error('Visualization must have a draw method.');
  }
};


/** Draws the last returned data table, if no data table exists, does nothing.*/
QueryWrapper.prototype.draw = function() {
  if (!this.currentDataTable) {
    return;
  }
  this.visualization.draw(this.currentDataTable, this.options);
};


QueryWrapper.prototype.sendAndDraw = function() {
  var query = this.query;
  var self = this;
  query.send(function(response) {self.handleResponse(response)});
};


/** Handles the query response returned by the data source. */
QueryWrapper.prototype.handleResponse = function(response) {
  this.currentDataTable = null;
  if (response.isError()) {
    this.handleErrorResponse(response);
  } else {
    this.currentDataTable = response.getDataTable();
    this.draw();
  }
};


/** Handles a query response error returned by the data source. */
QueryWrapper.prototype.handleErrorResponse = function(response) {
  var message = response.getMessage();
  var detailedMessage = response.getDetailedMessage();
  if (this.errorContainer) {
    google.visualization.errors.addError(this.errorContainer,
        message, detailedMessage, {'showInTooltip': false});
  } else {
    throw Error(message + ' ' + detailedMessage);
  }
};


/** Aborts the sending and drawing. */
QueryWrapper.prototype.abort = function() {
  this.query.abort();
};


//function ChartWrapper(chartType, dataSourceUrl, query, refreshInterval, options, containerId) {
function ChartWrapper(obj) {

	this.chartType = obj.chartType;
	this.dataSourceUrl = obj.dataSourceUrl;
	this.query = obj.query;
	this.query_object = null;
	this.refreshInterval = obj.refreshInterval;
	this.options = obj.options;
	this.containerId = obj.containerId;
	this.container = document.getElementById(this.containerId);

	command = "this.chart = new google.visualization." + this.chartType  + "(this.container)";
	eval(command);

	this.clear = function() {
		this.query_object && this.query_object.abort();
		this.chart.clearChart();
	}	

	this.draw = function() {
		this.query_object && this.query_object.abort();
		this.query_object = new google.visualization.Query(this.dataSourceUrl + "&tq=" + this.query);
		this.query_object.setRefreshInterval(this.refreshInterval);
		var queryWrapper = new QueryWrapper(this.query_object, this.chart, this.options, this.container);
		queryWrapper.sendAndDraw();
	}

	this.setRefreshInterval = function (refreshInterval) {
		this.refreshInterval = refreshInterval;
		this.query_object.setRefreshInterval(this.refreshInterval);
	}
}



// Function to plot latency vs time:

function drawLatencyPerTime(analysis_path) {
	var these_options = clone(default_options);
	//these_options.series = {0: {color: darkblue}}
	these_options.vAxis = {textStyle: {color: darkblue}, scaleType: 'linear', minValue:0, maxValue:20, textPosition: 'out' };
	these_options.title = 'Latency (ms)';

	latency_per_time_wrapper = new ChartWrapper({
		chartType: 'LineChart',
		dataSourceUrl: 'https://ldas-jobs.ligo-wa.caltech.edu/~patrick.godwin/cgi-bin/fx_data_server.cgi?tqx=reqId:0&dir=' + analysis_path ,
		query: '',
		refreshInterval: refresh,
		options: these_options, 
		containerId: 'latency_per_time_wrapper',
	});
	latency_per_time_wrapper.draw();
	charts.push(latency_per_time_wrapper);
}

// For new charts, increment reqID
  
  
  
// Function to plot missed messages vs time:

function drawPercentMissedPerTime(analysis_path) {
	var these_options = clone(default_options);
	//these_options.series = {0: {color: darkblue}}
	these_options.vAxis = {textStyle: {color: darkblue}, scaleType: 'linear', textPosition: 'out' };
	these_options.title = '% of channels missing per buffer';

	percent_missed_per_time_wrapper = new ChartWrapper({
		chartType: 'LineChart',
		dataSourceUrl: 'https://ldas-jobs.ligo-wa.caltech.edu/~patrick.godwin/cgi-bin/fx_data_server.cgi?tqx=reqId:1&dir=' + analysis_path ,
		query: '',
		refreshInterval: refresh,
		options: these_options, 
		containerId: 'percent_missed_per_time_wrapper',
	});
	percent_missed_per_time_wrapper.draw();
	charts.push(percent_missed_per_time_wrapper);
}

// Function to plot target SNR vs time:

function drawTargetSnrPerTime(analysis_path) {
	var these_options = clone(default_options);
	//these_options.series = {0: {color: darkblue}}
	these_options.vAxis = {textStyle: {color: darkblue}, scaleType: 'linear', textPosition: 'out' };
	these_options.title = 'H1:CAL-DELTAL_EXTERNAL_DQ SNR';

	target_snr_per_time_wrapper = new ChartWrapper({
		chartType: 'LineChart',
		dataSourceUrl: 'https://ldas-jobs.ligo-wa.caltech.edu/~patrick.godwin/cgi-bin/fx_data_server.cgi?tqx=reqId:2&dir=' + analysis_path ,
		query: '',
		refreshInterval: refresh,
		options: these_options, 
		containerId: 'percent_missed_per_time_wrapper',
	});
	target_snr_per_time_wrapper.draw();
	charts.push(targer_snr_per_time_wrapper);
}

